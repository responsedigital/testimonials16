<!-- Start Testimonials 16 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Testimonials with an image arranged in columns. -->
@endif
<div class="testimonials-16"  is="fir-testimonials-16">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="testimonials-16__wrap {{ $classes }}">
    @for($x = 0; $x < 3; $x++)
      <div class="testimonials-16__item">
        <img class="testimonials-16__image" src="{{ $x['image']['url'] ?: 'https://res.cloudinary.com/response-mktg/image/upload/q_auto,f_auto/v1594221943/fir/demos/img-placeholder.jpg' }}" alt="">
        <h4 class="testimonials-16__name">{{ $x['name'] ?: $faker->name }}</h4>
        <p class="testimonials-16__company">{{ $x['company'] ?: $faker->text($maxNbChars = 20) }}</p>
        <p class="testimonials-16__quote">{{ $x['quote'] ?: $faker->paragraph($nbSentences = 3, $variableNbSentences = true) }}</p>
      </div>
    @endfor
  </div>
</div>
<!-- End Testimonials 16 -->
