<?php

namespace Fir\Pinecones\Testimonials16;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Testimonials16',
            'label' => 'Pinecone: Testimonials 16',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Testimonials with an image arranged in columns."
                ],
                [
                    'label' => 'Testimonials',
                    'name' => 'testimonialsTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Testimonials',
                    'name' => 'testimonials',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'max' => 4,
                    'sub_fields' => [
                        [
                            'label' => 'Image',
                            'name' => 'image',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Person',
                            'name' => 'person',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Company',
                            'name' => 'company',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Quote',
                            'name' => 'quote',
                            'type' => 'textarea'
                        ]
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'numColumns',
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}
