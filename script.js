class Testimonials16 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.$ = $(this)
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initTestimonials16()
    }

    initTestimonials16 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Testimonials 16")
    }

}

window.customElements.define('fir-testimonials-16', Testimonials16, { extends: 'div' })
