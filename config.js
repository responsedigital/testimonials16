module.exports = {
    'name'  : 'Testimonials 16',
    'camel' : 'Testimonials16',
    'slug'  : 'testimonials-16',
    'dob'   : 'Testimonials_16_1440',
    'desc'  : 'Testimonials with an image arranged in columns.',
}